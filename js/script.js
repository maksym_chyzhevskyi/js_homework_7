/* Опишіть своїми словами як працює метод forEach.
Дозволяє перебирати всі елементи масиву з використаннм
фнкції callback, якій в свою чергу можуть
бути передані три параметри - елемент масиву, його номер та масив, що перебирається.

Як очистити масив?
За допомогою властивості length, задавши їй значення 0.

Як можна перевірити, що та чи інша змінна є масивом?
За допомогою методу isArray() - повертає якщо значення змінної є масивом
та false якщо ні.

*/

function filterBy(arr, type) {
    let newArr = arr.filter(item => typeof item !== type);
    return newArr;
}

const allTypes = ['string', 'number', 'boolean'];
allTypes.forEach(type => console.log(filterBy(['hello', 'world', 23, '23', 55, null], type)));